#include <Arduino.h>
#include <FastLED.h>
#include <ClickEncoder.h>
#include <TimerOne.h>

#define LED_PIN     3
#define COLOR_ORDER GRB
#define CHIPSET     NEOPIXEL
#define NUM_LEDS    56
#define HALF_LEDS 	(NUM_LEDS / 2)

#define DEBUG_LED0 0
#define DEBUG_LED1 1

#define BRIGHTNESS  140
#define BRIGHTNESS2 200
#define BRIGHTNESS3 250
#define FRAMES_PER_SECOND 30
#define DEFAULT_FADE 50;

#define ENC_A 4
#define ENC_B 5
#define ENC_BTN 6

#define MODE_BRIGHTNESS 0
#define MODE_SUBPATTERN 1

CRGB leds[NUM_LEDS];
CRGBPalette16 gPal;

bool gReverseDirection = false;
ClickEncoder *encoder;
int16_t encValue, encLastValue;
uint8_t gFade = DEFAULT_FADE;

/**********************************************************
 *
 * LEDTube
 *
 * Base functions
 *
 * 1)	differnet LEDPatterns
 * 2)	via quadraturencoder
 *		click
 *			next pattern
 *		double click
 *			change from mode1 to mode2 and back to mode1
 *			a double click is combined with a short flash of all leds
 *				=> 	green 	-> go to mode2
 *				=>	red		-> go to mode1
 *		turn cw
 *			mode1:	increase/decrease brightness
 *			mode2:	change configuration of current pattern (e. g. change colors)
 *
 * Pattern:
 * 	Fire
 * 		simulate a fire.
 *
 * 	AmbientLight
 *
 *********************************************************/



// List of patterns to cycle through.  Each is defined as a separate function below.
// Each pattern is defined as a function that takes two uint8_t's; all of the pattern functions
// have to have the same signature in this setup

typedef void (*MultipleArgumentPattern)(uint8_t arg0, uint8_t arg1, uint8_t arg2);
typedef struct {
	MultipleArgumentPattern mPattern;
	uint8_t mArg0;
	uint8_t mArg1;
	uint8_t mArg2;
} MultipleArgumentPatterWithArgumentValues;

MultipleArgumentPatterWithArgumentValues gPatternsAndArguments[] = {
		{ Fire, 0, 8, 0},
		{ DoubleFire, 0, 8, 30},
		//{ SmoothGradient, CRGB::LightYellow /*mode*/, 100 /*brightness*/, 10 /*speed*/},
		//{ SmoothAmbient, 0,0,2 /*offset*/},
		{ Crossfader, 90, 150, 0},
		{ Rainbow, 0, 4, 10},
		{ FixAmbient, 0, 0, 0},	// red
		{ FixAmbient, 32, 0, 0},	// orange
		{ FixAmbient, 64, 0, 0},	// yellow
		{ FixAmbient, 96, 0, 0},	// green
		{ FixAmbient, 128, 0, 0},	// aqua
		{ FixAmbient, 160, 0, 0},	// blue
		{ FixAmbient, 192, 0, 0},	// purple
		{ FixAmbient, 224, 0, 0},	// pink
		{ Gradient, 0, 32, 0},	// red-orange
		{ Gradient, 32, 64, 0},	// orange-yellow
		{ Gradient, 64, 96, 0},	// yellow-green
		{ Gradient, 96, 128, 0},	// green-aqua
		{ Gradient, 128, 160, 0},	// aqua-blue
		{ Gradient, 160, 192, 0},	// plue-purple
		{ Gradient, 192, 224, 0},	// purple-pink
		{ Gradient, 224, 255, 0},	// pink-red
};

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gCurrentBrightness = BRIGHTNESS;
uint8_t gHue = 0;
uint8_t gPatternSubMode= -1;
uint8_t gMode=0;

//The setup function is called once at startup of the sketch
void setup() {
	pinMode(DEBUG_LED0, OUTPUT);
	pinMode(DEBUG_LED1, OUTPUT);
	delay(500);
	DebugLED(DEBUG_LED0, 3, 200);
	DebugLED(DEBUG_LED1, 3, 200);
	delay(1000); // sanity delay
	encoder = new ClickEncoder(ENC_A, ENC_B, ENC_BTN, 1, LOW);
	encValue = encLastValue = BRIGHTNESS;

	Timer1.initialize(1000);
	Timer1.attachInterrupt(timerIsr);


	//TestEncoder();


	FastLED.addLeds<CHIPSET, LED_PIN>(leds, NUM_LEDS).setCorrection(0xFFB070).setDither(BRIGHTNESS < 255);
	FastLED.setBrightness( BRIGHTNESS);
	encoder->setAccelerationEnabled(false);

}

// The loop function is called in an endless loop
void loop() {
	encValue += encoder->getValue();
	gHue++;

	//
	// if system is in "normal" mode, with encoder it is possible to decrease/increase brightness
	//
	if (encValue != encLastValue) {
		DebugLED(DEBUG_LED0,1,20);
		if (encValue > 255) {
			encValue = 255;
		} else if (encValue < 0) {
			encValue = 0;
		}
		encLastValue = encValue;
		switch (gMode) {
		case MODE_BRIGHTNESS:
			gCurrentBrightness = encLastValue;
			FastLED.setBrightness(gCurrentBrightness);
			break;
		case MODE_SUBPATTERN:
			// next sub pattern mode
			if (encLastValue % 2 == 0) {
				gPatternSubMode++;
			}
			break;
		default:
			gMode=0;
		}
	}

	//
	// one click on button, set next pattern
	//
	ClickEncoder::Button b = encoder->getButton();
	if ( (b == ClickEncoder::Clicked)) {
		switch(gMode) {
		case MODE_BRIGHTNESS:
			DebugLED(DEBUG_LED1,1,20);
			FastLED.clear(true);
			delay(500);
			nextPattern();
			break;
		case MODE_SUBPATTERN:
			break;
		}
	}

	//
	// do a double click on pattern
	// system goes for current pattern into NEXT MODE
	//
	if (b == ClickEncoder::DoubleClicked) {
		if (gMode==0) {
			FlashStripe(1, CRGB::Green);
			DebugLED(DEBUG_LED1,1,20);
			gMode = 1;
		}
		else if (gMode == 1) {
			DebugLED(DEBUG_LED0,1,20);
			FlashStripe(1, CRGB::Red);
			gMode = 0;
		}
	}

	// Add entropy to random number generator; we use a lot of it.
	random16_add_entropy(random());

	// Call the current pattern function once, updating the 'leds' array
	uint8_t arg0 = gPatternsAndArguments[gCurrentPatternNumber].mArg0 + gPatternSubMode;
	uint8_t arg1 = gPatternsAndArguments[gCurrentPatternNumber].mArg1;
	uint8_t arg2 = gPatternsAndArguments[gCurrentPatternNumber].mArg2;
	MultipleArgumentPattern pat =
			gPatternsAndArguments[gCurrentPatternNumber].mPattern;

	pat(arg0, arg1, arg2);
	// send the 'leds' array out to the actual LED strip
	FastLED.show();

#if defined(FASTLED_VERSION) && (FASTLED_VERSION >= 2001000)
	FastLED.delay(1000 / FRAMES_PER_SECOND);
#else
	delay(1000 / FRAMES_PER_SECOND);
#endif  ﻿

}

//
// only for encoder TEST !!!
void TestEncoder() {
	while(1){
		encValue += encoder->getValue();
		if (encValue != encLastValue) {
			if (encValue > encLastValue)
				DebugLED(DEBUG_LED0,1,20);
			else
				DebugLED(DEBUG_LED1,1,20);
			encLastValue = encValue;
		}
	}
}

void DebugLED(uint8_t led, uint8_t count, uint8_t ms) {
	for (int x=0; x < count; x++) {
		digitalWrite((char)led, HIGH);
		delay(ms);
		digitalWrite((char)led, LOW);
		delay(ms);
	}
	digitalWrite((char)led, LOW);
}

void nextPattern() {
	// add one to the current pattern number, and wrap around at the end
	const int numberOfPatterns = sizeof(gPatternsAndArguments)
			/ sizeof(gPatternsAndArguments[0]);
	gCurrentPatternNumber = (gCurrentPatternNumber + 1) % numberOfPatterns;
}

//
// timer interrupt, used from encoder
//
void timerIsr() {
	encoder->service();
}

void FlashStripe(uint8_t count, CRGB c) {
	for (int x=0; x < NUM_LEDS; x++) {
		leds[x] = c;
	}
	for (int x=0; x < count; x++) {
		FastLED.show();
		FastLED.delay(250);
	}
	FastLED.clear(false);
	delay(100);
}


/**********************************************************************
 *
 * 	Pattern definitions
 *
 *
 *********************************************************************/
#define DENSITY 5

void Rainbow(uint8_t mode, uint8_t max_mode, uint8_t chanceOfGlitter) {
	if (gPatternSubMode < 0) gPatternSubMode = mode;
	if (gPatternSubMode > (max_mode-1)){
		gPatternSubMode = 0;
	}
	switch(gPatternSubMode){
	case 0:
		chanceOfGlitter = 0;
		break;
	case 1:
		chanceOfGlitter = random8()+30;
		break;
	case 2:
		chanceOfGlitter = random8()+100;
		break;
	case 3:
		chanceOfGlitter = random8()+150;
		break;
	default:
		chanceOfGlitter=0;
		gPatternSubMode = 0;
		if (encoder->getAccelerationEnabled()){
			FlashStripe(1,CRGB::Aqua);
		}
		break;
	}
	fill_rainbow( leds, NUM_LEDS, gHue, DENSITY);
	addGlitter(chanceOfGlitter);
}

void GradientPalette(uint8_t from1, uint8_t from2, uint8_t to1, uint8_t to2){
//	gPal = CRGBPalette16()
//
//	// Step 4.  Map from heat cells to LED colors
//	for (int j = 0; j < NUM_LEDS; j++) {
//		// Scale the heat value from 0-255 down to 0-240
//		// for best results with color palettes.
//		byte colorindex = scale8(heat[j], 240);
//		CRGB color = ColorFromPalette(gPal, colorindex);
//		int pixelnumber;
//		if (gReverseDirection) {
//			pixelnumber = (NUM_LEDS - 1) - j;
//		} else {
//			pixelnumber = j;
//		}
//		leds[pixelnumber] = color;
//	}

}

void SmoothAmbient(uint8_t mode, uint8_t max_mode, uint8_t offset) {
	int col = 0;
	col = (encValue+offset);
	if (col>255){
		col = offset;
	}
	FastLED.setBrightness(BRIGHTNESS3);
	fill_solid(leds, NUM_LEDS, CHSV(col, 255,255));
}

void SmoothGradient(uint8_t mode, uint8_t max_mode, uint8_t chanceOfGlitter) {
	int gHueFrom, gHueTo;
	if (gPatternSubMode < 0) gPatternSubMode = mode;
	if (gPatternSubMode > (max_mode-1)){
		gPatternSubMode = 0;
	}
	gHueFrom = 64;
	gHueTo = 32;
	if ((gPatternSubMode > 0)) {
		gHueTo = gHue;
		gHueFrom = gHue-48;
	}
	switch(gPatternSubMode){
	case 0:
		chanceOfGlitter = 0;
		break;
	case 1:
		chanceOfGlitter = random8()+10;
		break;
	case 2:
		chanceOfGlitter = random8()+20;
		break;
	case 3:
		chanceOfGlitter = random8()+100;
		break;
	default:
		chanceOfGlitter=0;
		gPatternSubMode = 0;
		if (encoder->getAccelerationEnabled()){
			FlashStripe(1,CRGB::Aqua);
		}
		break;
	}
	fill_gradient(leds, 0, CHSV(gHueFrom,255,gCurrentBrightness),NUM_LEDS, CHSV(gHueTo,255,gCurrentBrightness), SHORTEST_HUES);
	addGlitter(chanceOfGlitter);
}



#define SPEED 10
void Crossfader(uint8_t min_fade, uint8_t max_fade, uint8_t unused) {
	uint8_t fade = 100;
	if (fade < min_fade) {
		fade = max_fade;
		encValue = 0;
	}
	if (fade > max_fade) {
		fade = min_fade;
		encValue = 0;
	}
	fade += encValue;
	// a colored dot sweeping back and forth, with fading trails
	fadeToBlackBy( leds, NUM_LEDS, fade);
	int pos = beatsin16(SPEED, 0, NUM_LEDS);
	leds[pos] += CHSV( gHue, 255, 255);

}


// Fire2012 by Mark Kriegsman, July 2012
// as part of "Five Elements" shown here: http://youtu.be/knWiGsmgycY
//
// This basic one-dimensional 'fire' simulation works roughly as follows:
// There's a underlying array of 'heat' cells, that model the temperature
// at each point along the line.  Every cycle through the simulation,
// four steps are performed:
//  1) All cells cool down a little bit, losing heat to the air
//  2) The heat from each cell drifts 'up' and diffuses a little
//  3) Sometimes randomly new 'sparks' of heat are added at the bottom
//  4) The heat from each cell is rendered as a color into the leds array
//     The heat-to-color mapping uses a black-body radiation approximation.
//
// Temperature is in arbitrary units from 0 (cold black) to 255 (white hot).
//
// This simulation scales it self a bit depending on NUM_LEDS; it should look
// "OK" on anywhere from 20 to 100 LEDs without too much tweaking.
//
// I recommend running this simulation at anywhere from 30-100 frames per second,
// meaning an interframe delay of about 10-35 milliseconds.
//
//
// There are two main parameters you can play with to control the look and
// feel of your fire: COOLING (used in step 1 above), and SPARKING (used
// in step 3 above).
//
// COOLING: How much does the air cool as it rises?
// Less cooling = taller flames.  More cooling = shorter flames.
// Default 55, suggested range 20-100
#define COOLING  65

// SPARKING: What chance (out of 255) is there that a new spark will be lit?
// Higher chance = more roaring fire.  Lower chance = more flickery fire.
// Default 120, suggested range 50-200.
#define SPARKING 120

void Fire(uint8_t mode, uint8_t max_mode, uint8_t nix) {
	// Array of temperature readings at each simulation cell
	static byte heat[NUM_LEDS];
	if (gPatternSubMode < 0) gPatternSubMode = mode;
	if (gPatternSubMode > (max_mode-1)) {
		gPatternSubMode = 0;
	}
	switch (gPatternSubMode) {
	case 0:
		gPal = HeatColors_p;
		break;
	case 1:
		// These are other ways to set up the color palette for the 'fire'.
		// First, a gradient from black to red to yellow to white -- similar to HeatColors_p
		gPal = CRGBPalette16(CRGB::Black, CRGB::Red, CRGB::Yellow, 0xE0E0FF);
		break;
	case 2:
		// Second, this palette is like the heat colors, but blue/aqua instead of red/yellow
		gPal = CRGBPalette16( CRGB::Black, CRGB::Blue, CRGB::Aqua,  0xE0E0FF);
		break;
	case 3:
		// Third, here's a simpler, three-step gradient, from black to red to white
		gPal = CRGBPalette16( CRGB::Black, CRGB::Red, CRGB::White);
		break;
	case 4:
		// Fourth, inverse fire
		gPal = CRGBPalette16( CRGB::Black ^ CRGB::White, CRGB::Red ^ 0xE0E0FF, CRGB::Yellow ^ CRGB::White,  0xE0E0FF ^ CRGB::Black);
		break;
	case 5:
		// Fourth, inverse fire
		gPal = CRGBPalette16( CRGB::Red, CRGB::Black, CRGB::Green,  CRGB::Black);
		break;
	case 6:
		// Fourth, inverse fire
		gPal = CRGBPalette16( CRGB::Yellow, CRGB::Green, 0xE0E0FF, CRGB::Black);
		break;
	case 7:
		// These are other ways to set up the color palette for the 'fire'.
		// First, a gradient from black to red to yellow to white -- similar to HeatColors_p
		gPal = CRGBPalette16(CRGB::White, CRGB::Yellow, CRGB::Red, CRGB::Black);
		break;
	default:
		gPal = HeatColors_p;
		gPatternSubMode = 0;
		break;
	}
	// Step 1.  Cool down every cell a little
	for (int i = 0; i < NUM_LEDS; i++) {
		heat[i] = qsub8(heat[i], random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
	}

	// Step 2.  Heat from each cell drifts 'up' and diffuses a little
	for (int k = NUM_LEDS - 1; k >= 2; k--) {
		heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2]) / 3;
	}

	// Step 3.  Randomly ignite new 'sparks' of heat near the bottom
	if (random8() < SPARKING) {
		int y = random8(7);
		heat[y] = qadd8(heat[y], random8(160, 255));
	}

	// Step 4.  Map from heat cells to LED colors
	for (int j = 0; j < NUM_LEDS; j++) {
		// Scale the heat value from 0-255 down to 0-240
		// for best results with color palettes.
		byte colorindex = scale8(heat[j], 240);
		CRGB color = ColorFromPalette(gPal, colorindex);
		int pixelnumber;
		if (gReverseDirection) {
			pixelnumber = (NUM_LEDS - 1) - j;
		} else {
			pixelnumber = j;
		}
		leds[pixelnumber] = color;
	}

}


void DoubleFire(uint8_t mode, uint8_t max_mode, uint8_t delay){
	// Array of temperature readings at each simulation cell
	static byte heat[NUM_LEDS];


	if (gPatternSubMode < 0) gPatternSubMode = mode;
	if (gPatternSubMode > (max_mode-1)) {
		gPatternSubMode = 0;
	}
	switch (gPatternSubMode) {
	case 0:
		gPal = HeatColors_p;
		break;
	case 1:
		// These are other ways to set up the color palette for the 'fire'.
		// First, a gradient from black to red to yellow to white -- similar to HeatColors_p
		gPal = CRGBPalette16(CRGB::Black, CRGB::Red, CRGB::Yellow, 0xE0E0FF);
		break;
	case 2:
		// Second, this palette is like the heat colors, but blue/aqua instead of red/yellow
		gPal = CRGBPalette16( CRGB::Black, CRGB::Blue, CRGB::Aqua,  0xE0E0FF);
		break;
	case 3:
		// Third, here's a simpler, three-step gradient, from black to red to white
		gPal = CRGBPalette16( CRGB::Black, CRGB::Red, CRGB::White);
		break;
	case 4:
		// Fourth, inverse fire
		gPal = CRGBPalette16( CRGB::Black ^ CRGB::White, CRGB::Red ^ 0xE0E0FF, CRGB::Yellow ^ CRGB::White,  0xE0E0FF ^ CRGB::Black);
		break;
	case 5:
		// Fourth, inverse fire
		gPal = CRGBPalette16( CRGB::Red, CRGB::Black, CRGB::Green,  CRGB::Black);
		break;
	case 6:
		// Fourth, inverse fire
		gPal = CRGBPalette16( CRGB::Yellow, CRGB::Green, 0xE0E0FF, CRGB::Black);
		break;
	case 7:
		// These are other ways to set up the color palette for the 'fire'.
		// First, a gradient from black to red to yellow to white -- similar to HeatColors_p
		gPal = CRGBPalette16(CRGB::White, CRGB::Yellow, CRGB::Red, CRGB::Black);
		break;
	default:
		gPal = HeatColors_p;
		gPatternSubMode = 0;
		break;
	}
	// Step 1.  Cool down every cell a little
	for (int i = 0; i < HALF_LEDS-2; i++) {
		heat[i] = qsub8(heat[i], random8(0, ((COOLING * 10) / HALF_LEDS) + 2));
		heat[NUM_LEDS-i] = qsub8(heat[NUM_LEDS-i], random8(0, ((COOLING * 10) / HALF_LEDS) + 2));
	}

		// Step 2.  Heat from each cell drifts 'up' and diffuses a little
	for (int k = HALF_LEDS - 1; k >= 2; k--) {
		int x = NUM_LEDS - k;
		heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2]) / 3;
		heat[x] = (heat[x + 1] + heat[x + 2] + heat[x + 2]) / 3;
	}

	// Step 3.  Randomly ignite new 'sparks' of heat near the bottom
	int s = SPARKING - random8(SPARKING/2);
	if (random8() < s) {
		int z = random8(4);
		heat[NUM_LEDS - z] = qadd8(heat[NUM_LEDS - z], random8(160, 255));
	}

	if (random8() < s) {
		int y = random8(4);
		heat[y] = qadd8(heat[y], random8(160, 255));
	}


	// Step 4.  Map from heat cells to LED colors
	int k=0;
	for (int j = 0; j < HALF_LEDS; j++) {
		k = NUM_LEDS - j;
		// Scale the heat value from 0-255 down to 0-240
		// for best results with color palettes.
		byte colorindex1 = scale8(heat[j], 240);
		byte colorindex2 = scale8(heat[k], 240);
		CRGB color1 = ColorFromPalette(gPal, colorindex1);
		CRGB color2 = ColorFromPalette(gPal, colorindex2);
		int pixelnumber1, pixelnumber2;
		if (gReverseDirection) {
			pixelnumber1 = (HALF_LEDS - 1) - j;
			pixelnumber2 = (HALF_LEDS + 1) + k;
		} else {
			pixelnumber1 = j;
			pixelnumber2 = k;
		}
		leds[pixelnumber1] = color1;
		leds[pixelnumber2] = color2;
	}

	FastLED.delay(delay);
}

//
// add a random glitter led into the stripe
void addGlitter( fract8 chanceOfGlitter)
{
	if( random8() < chanceOfGlitter) {
//		leds[ random16(NUM_LEDS) ] += CRGB::White;
		leds[ random16(NUM_LEDS) ] += CHSV(gHue,255,255);
	}
}

//
// Farbtherapie - Pattern
void FixAmbient(uint8_t arg0, uint8_t arg1, uint8_t arg2) {
	// arg0 = color
	// arg1 = fade delay
	if (arg1 > 0) {
		fadeToBlackBy(leds,NUM_LEDS, gFade++);
		if (gFade > 255) {
			gFade = DEFAULT_FADE;
		}
	}
	//arg0 += gHue;
	fill_solid(leds, NUM_LEDS, CHSV(arg0, 255, 255));

}

void Gradient(uint8_t arg0, uint8_t arg1, uint8_t arg2){
	uint8_t hueFrom = arg0 + gHue;
	uint8_t hueTo = arg1 + gHue;
	fill_gradient(leds, 0, CHSV(hueFrom,255,gCurrentBrightness),NUM_LEDS, CHSV(hueTo,255,255), SHORTEST_HUES);

}
