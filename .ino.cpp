//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2015-12-12 11:43:01

#include "Arduino.h"
#include <Arduino.h>
#include <FastLED.h>
#include <ClickEncoder.h>
#include <TimerOne.h>
void setup() ;
void loop() ;
void TestEncoder() ;
void DebugLED(uint8_t led, uint8_t count, uint8_t ms) ;
void nextPattern() ;
void timerIsr() ;
void FlashStripe(uint8_t count, CRGB c) ;
void Rainbow(uint8_t mode, uint8_t max_mode, uint8_t chanceOfGlitter) ;
void GradientPalette(uint8_t from1, uint8_t from2, uint8_t to1, uint8_t to2);
void SmoothAmbient(uint8_t mode, uint8_t max_mode, uint8_t offset) ;
void SmoothGradient(uint8_t mode, uint8_t max_mode, uint8_t chanceOfGlitter) ;
void Crossfader(uint8_t min_fade, uint8_t max_fade, uint8_t unused) ;
void Fire(uint8_t mode, uint8_t max_mode, uint8_t nix) ;
void DoubleFire(uint8_t mode, uint8_t max_mode, uint8_t delay);
void addGlitter( fract8 chanceOfGlitter) ;
void FixAmbient(uint8_t arg0, uint8_t arg1, uint8_t arg2) ;
void Gradient(uint8_t arg0, uint8_t arg1, uint8_t arg2);


#include "FastLED_LampV2.ino"
